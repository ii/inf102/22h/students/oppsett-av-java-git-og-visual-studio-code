# Oppsett av Java, Git og Visual Studio Code
_Dette dokumentet beskriver hvordan du installerer programvaren du trenger for INF102 - høst 2022._

Målet med denne guiden er å installere og bli kjent med:

- [Java (og Maven)](#1-installere-java)
- [Kodeeditor (IDE)](#2-kodeeditoride)
- [Git](#3-git)
- [GitLab](#4-gitlab)

## 1) Installere Java
Vi bruker [Java](https://en.wikipedia.org/wiki/Java_(programming_language)) i kurset. Selv om de fleste elementer av Java vi kommer til å bruke i dette kurset har vært der siden Java 8, anbefaler vi å benytte Java 17, som er nyeste LTS (long-time support) -versjon for øyeblikket. **Merk**: Mange Java-brukere er kjent med Oracle sin kommersielle JDK, men i dette emnet vil vi bruke open source -alternativet OpenJDK.

* [Installere Java 17 for Windows](InstallereJavaWindows.md)
* [Installere Java 17 (og Maven 3.8) for Mac](InstallereJavaMac.md)

Linux-brukere kan også følge guiden for Mac.

### Kjøre Java i et terminalvindu
Når du har installert Java kan du kjøre et javaprogram fra terminalen (Windows: PowerShell).

Lag en ny fil på en valgfri lokasjon, med et valgfritt tekstredigeringsverktøy for ren tekst (f.eks. Notepad/Notisblokk i Windows, TextEdit for Mac, Gedit for Ubuntu/Linux eller lignende), og navngi filen `HelloWorld.java`. (Filnavnet skal **ikke** ende med .txt).

Inni filen kan du lime inn det følgende Java-programmet:

```
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
```

Fra terminalen kjører du HelloWorld.java ved å bruke kommandoen

```
> java HelloWorld.java
Hello World!
```

For å kjøre java-filen din fra terminalvinduet må du enten navigere dit den er, eller bruke fullstendig adresse. Det enkleste er ofte å navigere til riktig sted først. Hvis du prøver å kjøre den uten å være på riktig sted vil du få en feilmelding:

```
> java HelloWorld.java
Error: Could not find or load main class HelloWorld.java
Caused by: java.lang.ClassNotFoundException: HelloWorld.java
```

### Kommandoer for navigering i terminal
I terminal-vinduet befinner du deg til enhver tid i en mappe vi kaller *working directory*. For å navigere mellom mapper er det tre hendige kommandoer vi her introduserer helt kort:
- `pwd`<br/>
Denne kommandoen ("print working directory") vil vise deg hvilken mappe du befinner deg i for øyeblikket.
- `ls`<br/>
Denne kommandoen ("list") vil vise deg innholdet i mappen du befinner deg i.
- `cd mappenavn`<br/>
Denne kommadoen ("change directory") vil flytte deg *inn* i mappen ``mappenavn.'' For å gå *ut* av mappen du er i, bruk kommandoen `cd ..` (inkludert to punktum).

Kommandoene over vil virke i terminalene til Mac og Linux, samt i PowerShell og Git Bash for Windows. *For Windows CMD (ledetekst/command prompt) gjelder det andre kommandoer, så bruk PowerShell for å følge denne guiden.*

✅ **Når du får til å skrive ut `Hello World!` fra terminalen kan du gå videre.**

### Feilsøking
 - Kommandoene `pwd`, `ls` og `cd` vil ikke ødelegge, slette eller endre noe på datamaskinen din. De er derfor trygge å eksperimentere med.
 - TextEdit vil ofte lage nye filer i RTF-format (hvor tekst kan ha ulike fonter, skriftstørrelse, fet skrift og lignende, slik som Word-filer). Når du skriver kode ønsker du istedet å bruke *rene tekstfiler*. Gå til innstillinger i TextEdit og velg ren tekstfil (plain text), og opprett så en ny fil som du bruker herfra.
 
## 2) Kodeeditor/IDE
For å hjelpe oss å skrive kode benytter vi oss av en IDE (Integrated Development Environment). For Java-utvikling er det tre IDE'er vi anbefaler: Eclipse, Visual Studio Code og IntelliJ. I forelesningene kommer hovedsakelig Eclipse og VS Code til å bli brukt, men hvilken som helst vil fungere for deres egen bruk.

### Visual Studio Code
[Visual Studio Code](https://en.wikipedia.org/wiki/Visual_Studio_Code) er en relativt nyutviklet IDE som er utviklet for å kunne brukes med mange ulike språk. Den ble først utgitt av Microsoft i 2015, og har på kort tid blitt enormt populær: i 2021 var dette den foretrukne IDE blant hele 70% av brukerne på StackOverflow.

De som har fullført INF100 de siste to årene burde allerede ha Visual Studio Code installert. Hvis ikke kan du laste det ned her: [https://code.visualstudio.com/download](https://code.visualstudio.com/download).

>Tips til Mac/Linux-brukere: Installer VS Code med kommandoen `brew install --cask visual-studio-code` (homebrew må installeres først). Dette vil også installere den hendige *code* -kommandoen på systemet ditt, som lar deg åpne VS Code enkelt fra terminalen. Hvis du ikke installerte via homebrew, se https://stackoverflow.com/questions/30065227/run-open-vscode-from-mac-terminal for en guide for å installere *code*. For Windows-brukere skal *code* virke automatisk.


For å hjelpe oss skrive Java-kode vil vi installere en gruppe med utvidelser kalt **"Extension pack for Java"**. Inne i Visual Studio Code trykk på "Extensions" i kolonnen til venstre (evt. menyvalg *View -> Extensions*) og søk "Extension pack for Java". Trykk på `Install`.

### Eclipse
[Eclipse](<https://en.wikipedia.org/wiki/Eclipse_(software)>) er en annen IDE som er populær blant Java-utviklere. Utgitt først i 2001 av IBM er dette en IDE som er spesialdesignet for Java. Den er også i stor grad utviklet i Java selv. Dette er en IDE som har vært populær over lang tid, og som har et enormt utvalg av plugins for Java-utvikling. Det er også den foretrukne Java-IDE for emneansvarlig Martin.

Du kan laste ned *Eclipse IDE for Java Developers* her: https://www.eclipse.org/downloads/packages/release/2021-12/r/eclipse-ide-java-developers.

*Du må installere* ***Eclipse for Java Developers,*** *ikke Java EE Developers, C/C++, etc.*

Velg riktig mappe for Java 17-installasjonen din før du trykker på INSTALL. Hvis du ikke ser Java 17 i nedtrekksmenyen kan du finne riktig mappe ved å gå til terminalen din og skrive kommandoen `which java` i Linux/Unix/Mac OS X eller `gcm java` i Windows PowerShell (Alternativer for Windows: se https://superuser.com/a/367408).

Når du er ferdig å installere Eclipse kan du åpne den og velge et passende navn for ditt workspace. F.eks.: "workspace - INF102h22".

Når Eclipse allerede er åpen finner du versjons-informasjon i menyen under Eclipse -> About Eclipse for Mac og Help -> About Eclipse for Windows.
Mer informasjon om å [installere Eclipse](https://www.eclipse.org/downloads/packages/installer)

## 3) Git

<!-- GitLab er en av mange git-servere som gjør det enkelt for flere å samarbeide på et kodeprosjekt ved å bruke git. -->
[Git](https://en.wikipedia.org/wiki/Git) er et verktøy for *versjonskontroll* som hjelper deg å ta vare på (ulike versjoner av) koden. Du kan finne tilbake til forrige versjon og skulle du miste noe data på PCen din så ligger alt i backup på en server.

Vi bruker git for å levere ut oppgavekode i dette kurset, og du må bruke git for å levere inn laber og oppgaver (slik som denne). Git er meget populært i næringslivet, og er et verktøy du får mye igjen for å lære deg skikkelig. De virkelig store fordelene med git vil åpenbare seg når dere er flere som samarbeider på samme prosjekt, som f. eks. i INF112. Enn så lenge holder det å bli kjent med de grunnleggende funksjonene.

### Installere git

For de som foretrekker video over tekst har vi en [installasjonsguide](https://www.youtube.com/watch?v=lw3Vz6WsomM):

<a href="https://youtu.be/lw3Vz6WsomM"><img src="https://img.youtube.com/vi/lw3Vz6WsomM/maxresdefault.jpg" alt="Git Guide - Institutt for Informatikk" style="width:1px;" width=500px></a>

Sjekk om du har git installert ved å skrive `git` i et terminalvindu. Dersom du har git installert vil du få `usage: git` og en oversikt over mulige argumenter. Dersom du får `command not found` eller tilsvarende må du installere git.

```
> git
command not found: git
```

Du hente git herfra: https://git-scm.com/downloads. Velg ditt operativsystem og følg instruksjonene.

Når du er ferdig med installasjonen kan du kjøre `git` kommandoen igjen, og sjekke at du får oversikt over git kommandoer.

```
> git
usage: git [--version] [--help] [-C <path>] [-c name=value]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | --no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]
```

✅ **Når kommandoen `git` gir deg utskrift lignende den vist over kan du gå videre.**

### Sette git brukernavn og epost

Det er lurt å sette brukernavn og passord for git, ellers vil den klage i tide og utide. Bruk kommandoene:

```
git config --global user.name "Mona Lisa"
git config --global user.email "email@example.com"
```


## 4) GitLab

UiB har sin egen GitLab -server. For å få tilgang til den må du opprette konto der.

Gå til [https://git.app.uib.no/](https://git.app.uib.no/) og logg inn med Dataporten.

**VIKTIG: Ikke opprett bruker med Github! Disse brukerne mangler nødvendige rettigher og vil ikke kunne levere de obligatoriske oppgavene.**

<img src="images/noGithub.png" height="150"/>

<!-- Logg inn med Dataporten, **IKKE MED Github**. Brukere med Github vil ikke gis tilgang.
Da blir en konto laget for deg på Gitlab. Viktig: du skal ikke endre det automatiske brukernavnet du får.
Hvis du endrer brukernavn vil du ikke få tilgang til øvelsene i kurset. -->

### Sette opp ssh key

Det finnes flere måter å logge inn når man bruker git, men det å skrive brukernavn og passord hver gang man har gjort noen endringer er unødvendig.
En ssh key lagres på PCen slik at hver gang du ber om tilkobling til en server så sjekkes ssh key istedenfor å be om passord.

For å opprette ssh-ky følg denne guiden: https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/How-to-configure-GitLab-SSH-keys-for-secure-Git-connections, eller se [video lenket ovenfor](https://www.youtube.com/watch?v=lw3Vz6WsomM) fra 3:18. 

#### Feilsøking

En ssh key lagres på datamaskinen, hvis du har 2 maskiner må du gjøre dette på begge.

Du må vise "skjulte" filer og mapper for å kunne se mappen `.ssh`. 
